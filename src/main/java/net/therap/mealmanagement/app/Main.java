package net.therap.mealmanagement.app;

import net.therap.mealmanagement.controller.CommandParser;
import net.therap.mealmanagement.util.HibernateUtil;
import net.therap.mealmanagement.util.PasswordUtil;
import net.therap.mealmanagement.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class Main {

    private static final String INSTRUCTIONS = "Example command:\n" +
            "To view all: view all\n" +
            "To view a particular slot: view sunday breakfast\n" +
            "To add: add sunday lunch fish\n" +
            "To delete: delete sunday lunch beef\n" +
            "To update: update sunday lunch beef chicken\n" +
            "To add review: review-add sunday lunch 5.0 too good!\n" +
            "To view review (all): review-view all\n" +
            "To view review (by user): review-view username\n" +
            "To view review (by meal): review-view sunday lunch\n";

    public static void main(String[] args) throws IOException {

        View view = new View();
        HibernateUtil util = new HibernateUtil();
        CommandParser controller = new CommandParser(view, util);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            while (true) {
                System.out.println("username: ");
                String username = reader.readLine();
                System.out.println("password: ");
                String password = reader.readLine();

                if (controller.authentiateUser(username, PasswordUtil.getHashedPassword(password))) {
                    System.out.println("Login successful");
                    break;
                } else {
                    System.out.println("Incorrect login or password. Try again!");
                }
            }

            System.out.println(INSTRUCTIONS);

            while (true) {
                if (reader.ready()) {
                    String command = reader.readLine();

                    if (command.equals(CommandParser.EXIT_COMMAND)) {
                        break;
                    }
                    controller.parse(command);
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        } finally {
            controller.closeManager();
        }
    }
}
