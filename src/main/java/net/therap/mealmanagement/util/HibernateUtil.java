package net.therap.mealmanagement.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Level;


/**
 * @author shahriar
 * @since 2/14/18
 */
public class HibernateUtil {

    private static final String PERSISTENCE_UNIT_NAME = "persistence";

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public HibernateUtil() {
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
        entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        entityManager = entityManagerFactory.createEntityManager();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void closeEntityManager() {
        if (entityManager != null && entityManager.isOpen()) {
            entityManager.close();
        }

        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
    }
}
