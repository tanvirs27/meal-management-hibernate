package net.therap.mealmanagement.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author shahriar
 * @since 2/11/18
 */

@Entity
@Table(name = "meal")
public class Meal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "day")
    private String day;

    @Column(name = "slot")
    private String slot;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "meal_item", joinColumns = @JoinColumn(name = "meal_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private List<Item> items;

    @OneToMany(mappedBy = "meal", cascade = CascadeType.ALL)
    private List<Review> reviews;

    @OneToOne
    @JoinColumn(name = "modified_by")
    private User modifiedBy;

    @Column(name = "modification_time")
    private Timestamp lastModificationTime;

    public int getId() {
        return id;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Timestamp getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(Timestamp lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    private String printFormatter() {
        String string = "";
        string += day + " " + slot + "\n";
        string += "Modified by: " + modifiedBy.getUsername() + "\n";
        string += "Modification Time: " + lastModificationTime + "\n\n";

        for (Item item : items) {
            string += item + "\n";
        }

        return (items.size() > 0) ? string : string + "No item available\n";
    }

    public String toString() {
        return printFormatter();
    }
}
