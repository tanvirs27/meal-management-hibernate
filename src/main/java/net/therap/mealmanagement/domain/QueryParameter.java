package net.therap.mealmanagement.domain;

import java.io.Serializable;

/**
 * @author shahriar
 * @since 2/18/18
 */
public class QueryParameter implements Serializable{

    private static final long serialVersionUID = 1L;

    private String name;
    private Object value;

    public QueryParameter(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
