package net.therap.mealmanagement.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author shahriar
 * @since 2/15/18
 */
@Entity
@Table(name = "review")
public class Review implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinTable(name = "user_review", joinColumns = @JoinColumn(name = "review_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User reviewer;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinTable(name = "meal_review", joinColumns = @JoinColumn(name = "review_id"),
            inverseJoinColumns = @JoinColumn(name = "meal_id"))
    private Meal meal;

    @Column(name = "comment")
    private String comment;

    @Column(name = "rating")
    private double rating;

    @Column(name = "time")
    private Timestamp time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public User getReviewer() {
        return reviewer;
    }

    public void setReviewer(User reviewedBy) {
        this.reviewer = reviewedBy;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String printFormatter() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Reviewer: " + reviewer.getUsername() + "\n");
        stringBuilder.append("Review Time: " + time + "\n");
        stringBuilder.append("Meal: " + meal.getDay() + " " + meal.getSlot() + "\n");
        stringBuilder.append("Rating: " + rating + "\n");
        stringBuilder.append("Comment: " + comment + "\n");

        return stringBuilder.toString();
    }

    public String toString() {
        return printFormatter();
    }
}
