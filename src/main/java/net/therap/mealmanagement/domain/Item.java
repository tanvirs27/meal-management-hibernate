package net.therap.mealmanagement.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */

@Entity
@Table(name = "item")
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "items", cascade = CascadeType.MERGE)
    private List<Meal> meals;

    @OneToOne
    @JoinColumn(name = "added_by")
    private User addedBy;

    @Column(name = "addition_time")
    private Timestamp additionTime;

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(User addedBy) {
        this.addedBy = addedBy;
    }

    public Timestamp getAdditionTime() {
        return additionTime;
    }

    public void setAdditionTime(Timestamp additionTime) {
        this.additionTime = additionTime;
    }

    public String printFormatter() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Item name: " + name + "\n");
        stringBuilder.append("Added By: " + addedBy.getUsername() + "\n");
        stringBuilder.append("Addition Time: " + additionTime + "\n");

        return stringBuilder.toString();
    }

    public String toString() {
        return printFormatter();
    }
}
