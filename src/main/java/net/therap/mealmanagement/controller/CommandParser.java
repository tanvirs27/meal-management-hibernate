package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.dao.ReviewDao;
import net.therap.mealmanagement.dao.UserDao;
import net.therap.mealmanagement.domain.Meal;
import net.therap.mealmanagement.domain.Review;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.HibernateUtil;
import net.therap.mealmanagement.util.PasswordUtil;
import net.therap.mealmanagement.view.View;

import javax.persistence.EntityManager;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/11/18
 */
public class CommandParser {

    public static final String VIEW_COMMAND = "view";
    public static final String VIEW_ALL = "all";
    public static final String ADD_COMMAND = "add";
    public static final String DELETE_COMMAND = "delete";
    public static final String UPDATE_COMMAND = "update";
    public static final String EXIT_COMMAND = "exit";
    public static final String REVIEW_ADD_COMMAND = "review-add";
    public static final String REVIEW_VIEW_COMMAND = "review-view";

    public static final int MIN_COMMAND_LENGTH_FOR_VIEW = 3;
    public static final int MIN_COMMAND_LENGTH_FOR_ADD_DEL = 4;
    public static final int MIN_COMMAND_LENGTH_FOR_UPDATE = 5;
    public static final int MIN_COMMAND_LENGTH_FOR_REVIEW_ADD = 5;

    public static final int COMMAND_LENGTH_FOR_REVIEW_VIEW_MEAL = 3;
    public static final int COMMAND_LENGTH_FOR_REVIEW_VIEW_USER = 2;

    public static final String ERROR_ARGUMENT = "Too few arguments";
    public static final String SUCCESS_MSG = "succeeded!";

    private MealDao mealDao;
    private UserDao userDao;
    private ReviewDao reviewDao;
    private User user;
    private View view;
    private HibernateUtil util;
    private EntityManager entityManager;

    public CommandParser(View view, HibernateUtil util) {
        this.util = util;
        this.entityManager = util.getEntityManager();

        this.mealDao = new MealDao(entityManager);
        this.userDao = new UserDao(entityManager);
        this.reviewDao = new ReviewDao(entityManager);
        this.view = view;
    }

    public boolean authentiateUser(String username, String password) {

        user = userDao.getUser(username, password);
        mealDao.setCurrentUser(user);
        reviewDao.setCurrentUser(user);
        return user != null;
    }

    public void addDummyUser() throws NoSuchAlgorithmException {

        User userToAdd = new User();
        userToAdd.setUsername("shahriar");
        userToAdd.setDesignation("Associate Software Engineer");
        userToAdd.setEmail("shahriar@therapservices.net");
        userToAdd.setFullName("Tanvir Shahriar Rifat");
        userToAdd.setPassword(PasswordUtil.getHashedPassword("rifat007"));

        List<Review> reviews = new ArrayList<>();

        userToAdd.setAllReviews(reviews);

        userDao.addUser(userToAdd);
    }

    public void parse(String command) {

        if (command.equals("")) {
            return;
        }

        String[] splittedCommand = command.toLowerCase().split(" ");

        String commandName = splittedCommand[0];

        switch (commandName) {
            case VIEW_COMMAND:
                if (splittedCommand[1].equals(VIEW_ALL)) {
                    view.printList(mealDao.getAll());
                } else if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_VIEW) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    view.printList(mealDao.getMealBySlot(splittedCommand[1], splittedCommand[2]));
                }
                break;

            case ADD_COMMAND:
                if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_ADD_DEL) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    mealDao.addItemToMeal(splittedCommand[1], splittedCommand[2], splittedCommand[3]);
                    view.printLine(SUCCESS_MSG);
                }
                break;

            case DELETE_COMMAND:
                if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_ADD_DEL) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    mealDao.deleteItemFromMeal(splittedCommand[1], splittedCommand[2], splittedCommand[3]);
                    view.printLine(SUCCESS_MSG);
                }
                break;

            case UPDATE_COMMAND:
                if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_UPDATE) {
                    view.printLine(ERROR_ARGUMENT);
                } else {
                    mealDao.updateItemInMeal(splittedCommand[1], splittedCommand[2],
                            splittedCommand[3], splittedCommand[4]);
                    view.printLine(SUCCESS_MSG);
                }
                break;

            case REVIEW_ADD_COMMAND:
                if (splittedCommand.length < MIN_COMMAND_LENGTH_FOR_REVIEW_ADD) {
                    view.printLine(ERROR_ARGUMENT);
                } else {

                    Meal meal = mealDao.getMeal(splittedCommand[1], splittedCommand[2]);

                    double rating;
                    try {
                        rating = Double.parseDouble(splittedCommand[3]);
                    } catch (NumberFormatException e) {
                        rating = 0;
                    }

                    StringBuilder comment = new StringBuilder();
                    for (int i = 4; i < splittedCommand.length; i++) {
                        comment.append(splittedCommand[i]);
                        comment.append(" ");
                    }

                    reviewDao.addReviewToMeal(meal, rating, comment.toString());
                    view.printLine(SUCCESS_MSG);
                }
                break;

            case REVIEW_VIEW_COMMAND:
                if (splittedCommand[1].equals(VIEW_ALL)) {
                    view.printList(reviewDao.getAll());
                } else if (splittedCommand.length == COMMAND_LENGTH_FOR_REVIEW_VIEW_USER) {
                    view.printList(reviewDao.getReviewByUser(splittedCommand[1]));
                } else if (splittedCommand.length == COMMAND_LENGTH_FOR_REVIEW_VIEW_MEAL) {
                    view.printList(reviewDao.getReviewByMeal(splittedCommand[1], splittedCommand[2]));
                } else {
                    view.printLine(ERROR_ARGUMENT);
                }
                break;

            default:
                break;
        }
    }

    public void closeManager() {
        util.closeEntityManager();
    }
}
