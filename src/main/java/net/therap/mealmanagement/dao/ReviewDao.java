package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Meal;
import net.therap.mealmanagement.domain.Review;
import net.therap.mealmanagement.domain.User;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/18/18
 */
public class ReviewDao extends GenericDao<Review> {

    private ItemDao itemDao;
    private MealDao mealDao;
    private UserDao userDao;
    private User currentUser;

    public ReviewDao(EntityManager entityManager) {
        super(entityManager, Review.class);
        this.itemDao = new ItemDao(entityManager);
        this.mealDao = new MealDao(entityManager);
        this.userDao = new UserDao(entityManager);
    }

    public void addReviewToMeal(Meal meal, double rating, String comment) {

        Review review = new Review();

        review.setComment(comment);
        review.setRating(rating);
        review.setMeal(meal);
        review.setReviewer(currentUser);
        review.setTime(new Timestamp(System.currentTimeMillis()));

        update(review);
    }

    public List<Review> getReviewByUser(String username) {

        User user = userDao.getByUsername(username);

        return (user != null) ? user.getAllReviews() : new ArrayList<>();
    }

    public List<Review> getReviewByMeal(String day, String slot) {

        Meal meal = mealDao.getMeal(day, slot);

        return meal.getReviews();
    }

    public void setCurrentUser(User currentUser) {

        this.currentUser = currentUser;
        itemDao.setCurrentUser(currentUser);
        mealDao.setCurrentUser(currentUser);
    }
}
