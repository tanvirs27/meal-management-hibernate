package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.QueryParameter;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author shahriar
 * @since 2/18/18
 */
public class GenericDao<T> {

    private EntityManager entityManager;
    private Class<T> typeParameterClass;

    public GenericDao(EntityManager entityManager, Class<T> typeParameterClass) {
        this.entityManager = entityManager;
        this.typeParameterClass = typeParameterClass;
    }

    public List<T> getAll() {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(typeParameterClass);
        Root<T> root = criteriaQuery.from(typeParameterClass);

        criteriaQuery.select(root);

        TypedQuery<T> query = entityManager.createQuery(criteriaQuery);

        return query.getResultList();
    }

    public List<T> getByProperty(List<QueryParameter> parameters) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(typeParameterClass);
        Root<T> root = criteriaQuery.from(typeParameterClass);

        criteriaQuery.select(root);

        QueryParameter firstParameter = parameters.get(0);
        Predicate finalPredicate = criteriaBuilder.equal(root.get(firstParameter.getName()), firstParameter.getValue());

        for (int i = 1; i < parameters.size(); i++) {

            QueryParameter parameter = parameters.get(i);
            Predicate presentPredicate = criteriaBuilder.equal(root.get(parameter.getName()), parameter.getValue());

            finalPredicate = criteriaBuilder.and(finalPredicate, presentPredicate);
        }

        criteriaQuery.where(finalPredicate);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public void update(T object) {
        entityManager.getTransaction().begin();

        entityManager.merge(object);

        entityManager.getTransaction().commit();
    }

    public void save(T object) {
        entityManager.getTransaction().begin();

        entityManager.persist(object);

        entityManager.getTransaction().commit();
    }

    public void delete(T object) {
        entityManager.getTransaction().begin();

        entityManager.remove(object);

        entityManager.getTransaction().commit();
    }
}
