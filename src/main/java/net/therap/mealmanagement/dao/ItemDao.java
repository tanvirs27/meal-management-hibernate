package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Item;
import net.therap.mealmanagement.domain.QueryParameter;
import net.therap.mealmanagement.domain.User;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class ItemDao extends GenericDao<Item> {

    private User currentUser;

    public ItemDao(EntityManager entityManager) {
        super(entityManager, Item.class);
    }

    public Item getItem(String name) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("name", name));

        List<Item> results = getByProperty(parameters);

        Item item;

        if (results.size() > 0) {
            item = results.get(0);
        } else {
            item = new Item();
            item.setName(name);
            item.setMeals(new ArrayList<>());
            item.setAddedBy(currentUser);
            item.setAdditionTime(new Timestamp(System.currentTimeMillis()));
        }

        return item;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
