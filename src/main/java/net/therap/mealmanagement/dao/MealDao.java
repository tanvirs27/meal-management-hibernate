package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Item;
import net.therap.mealmanagement.domain.Meal;
import net.therap.mealmanagement.domain.QueryParameter;
import net.therap.mealmanagement.domain.User;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class MealDao extends GenericDao<Meal> {

    private ItemDao itemDao;
    private User currentUser;

    public MealDao(EntityManager entityManager) {
        super(entityManager, Meal.class);
        this.itemDao = new ItemDao(entityManager);
    }

    public List<Meal> getMealBySlot(String day, String slot) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("day", day));
        parameters.add(new QueryParameter("slot", slot));

        return getByProperty(parameters);
    }

    public void addItemToMeal(String day, String slot, String itemName) {

        Meal meal = getMeal(day, slot);

        Item item = itemDao.getItem(itemName);

        List<Item> allItems = meal.getItems();
        allItems.add(item);
        meal.setItems(allItems);

        update(meal);
    }

    public void deleteItemFromMeal(String day, String slot, String itemName) {

        Meal meal = getMeal(day, slot);

        Item item = itemDao.getItem(itemName);

        List<Item> allItems = meal.getItems();
        allItems.remove(item);
        meal.setItems(allItems);
        meal.setModifiedBy(currentUser);
        meal.setLastModificationTime(new Timestamp(System.currentTimeMillis()));

        update(meal);
    }


    public void updateItemInMeal(String day, String slot, String changedFrom, String changedTo) {

        Meal meal = getMeal(day, slot);

        Item previousItem = itemDao.getItem(changedFrom);
        Item presentItem = itemDao.getItem(changedTo);

        List<Item> allItems = meal.getItems();
        allItems.remove(previousItem);
        allItems.add(presentItem);
        meal.setItems(allItems);
        meal.setModifiedBy(currentUser);
        meal.setLastModificationTime(new Timestamp(System.currentTimeMillis()));

        update(meal);
    }

    public Meal getMeal(String day, String slot) {

        List<Meal> results = getMealBySlot(day, slot);

        Meal meal;

        if (results.size() > 0) {
            meal = results.get(0);
        } else {
            meal = new Meal();
            meal.setDay(day);
            meal.setSlot(slot);
            meal.setItems(new ArrayList<>());
            meal.setReviews(new ArrayList<>());
            meal.setModifiedBy(currentUser);
            meal.setLastModificationTime(new Timestamp(System.currentTimeMillis()));
        }

        return meal;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
        itemDao.setCurrentUser(currentUser);
    }
}
