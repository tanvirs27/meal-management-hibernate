package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.QueryParameter;
import net.therap.mealmanagement.domain.User;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/15/18
 */
public class UserDao extends GenericDao<User> {

    public UserDao(EntityManager entityManager) {
        super(entityManager, User.class);
    }

    public User getUser(String username, String password) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("username", username));
        parameters.add(new QueryParameter("password", password));

        List<User> results = getByProperty(parameters);

        return (results.size() > 0) ? results.get(0) : null;
    }

    public User getByUsername(String username) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("username", username));

        List<User> results = getByProperty(parameters);

        return (results.size() > 0) ? results.get(0) : null;
    }


    public void addUser(User user) {
        update(user);
    }
}
