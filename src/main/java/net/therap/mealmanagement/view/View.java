package net.therap.mealmanagement.view;

import java.util.List;

/**
 * @author shahriar
 * @since 2/11/18
 */
public class View {

    public <T> void printList(List<T> list) {
        for (T item : list) {
            System.out.println(item);
        }
    }

    public void printLine(String line) {
        System.out.println(line);
    }
}
